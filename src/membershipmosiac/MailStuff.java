package membershipmosiac;

import java.io.File;
import java.util.Properties;

import javax.mail.Message;
//import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import java.util.*;
import javax.mail.*;
import javax.mail.Flags.Flag;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

//import processing.core.*;
//import processing.core.PApplet;
//import processing.core.PImage;

public class MailStuff {
	String filename = "C:/mymosaic.jpg";

	String FROM = "SmithsonianMosaic@si-exhibit.net";   // Replace with your "From" address. This address must be verified.
	String TO = "shudson@bostonproductions.com";  // Replace with a "To" address. If you have not yet requested
	// production access, this address must be verified.

	String BODY = "Here's your mosaic!";
	String SUBJECT = "Mosaic";

	// Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.
	String SMTP_USERNAME = "AKIAI6GJEFC2EJEBYFDA";  // Replace with your SMTP username.
	String SMTP_PASSWORD = "AsmIfmVFdJ39Q1y4XWGsiKc586ZVhix7XGNHTZSw3Uts";  // Replace with your SMTP password.

	// Amazon SES SMTP host name. This example uses the us-east-1 region.
	String HOST = "email-smtp.us-east-1.amazonaws.com"; 
	String replytoemail = "giving@si.edu";   
	String senderName = "Smithsonian";
	boolean useSMTP = false;

	// Port we will connect to on the Amazon SES SMTP endpoint.
	int PORT = 25;

	String filePath = "C:/Users/Sarah Hudson/Documents/Processing/imageCollage/output/";

	// A function to check a mail account - IMAP
	void sendMail(String fn, String em, String fr, String un, String pw, String ho, int po, String su, String bo, String fp, String sen, String rep, boolean sm) {
	  filename = fn;
	  TO = em;
	  FROM = fr;
	  BODY = bo;
	  SUBJECT = su;
	  SMTP_USERNAME = un;
	  SMTP_PASSWORD = pw;
	  HOST = ho;
	  PORT = po;
	  filePath = fp;
	  senderName = sen;
	  replytoemail = rep;
	  useSMTP = sm;
	  try {
	    try 
	    {
	      Properties props = new Properties();
	      props.setProperty("mail.transport.protocol", "smtp");
	      props.put("mail.smtp.port", PORT); 
	      if (useSMTP == false)
	      {
	    	  props.put("mail.smtp.auth", "false");
	      }
	      else
	      {
	    	  props.put("mail.smtp.auth", "true");
	      }
	      props.put("mail.smtp.starttls.enable", "false");
	      props.put("mail.smtp.starttls.required", "false");
	      props.setProperty("mail.aws.user", SMTP_USERNAME);
	      props.setProperty("mail.aws.password", SMTP_PASSWORD);

	      Session session = Session.getDefaultInstance(props);

	      MimeMessage msg = new MimeMessage(session);
	      msg.setFrom(new InternetAddress(FROM, senderName));
	      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(TO));
	      msg.setSubject(SUBJECT);
	      msg.setReplyTo(InternetAddress.parse(replytoemail));
	      MimeBodyPart messageBodyPart = new MimeBodyPart();
	      messageBodyPart.setText(BODY, "utf-8", "html");
	      msg.saveChanges();

	      MimeMultipart multipart = new MimeMultipart();

	      multipart.addBodyPart(messageBodyPart);

	      messageBodyPart = new MimeBodyPart();

	      File f = new File(filePath + "/" + filename);
	      System.out.println("File path: " + filePath);
	      System.out.println("filename: " + filename);

	      MimeBodyPart attachmentPart = new MimeBodyPart();
	      attachmentPart.attachFile(f);
	      multipart.addBodyPart(attachmentPart);

	      msg.setContent(multipart);
	      msg.saveChanges();

	      // Reuse one Transport object for sending all your messages for better performance
	      Transport t = session.getTransport();
	      t.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
	      t.sendMessage(msg, msg.getAllRecipients());
	      //println("Email sent!");

	      // Close your transport when you're completely done sending all your messages.
	      t.close();
	    }
	    finally 
	    {
	     
	    }
	  }
	  catch (Exception e) {
	    System.out.println("Failed to connect to the store");
	    e.printStackTrace();
	  }
	}
}
