package membershipmosiac;

import java.util.ArrayList;
import java.util.Vector;
import java.util.function.Function;

import processing.core.PApplet;
import processing.core.PImage;


public class MosaicState {

	public static ArrayList<Box> shuffledBoxes;
	public static Photo[] sourceArray;
	private static ArrayList<PImage> imageArray;
	private static int[] mostRecentIndices;
	private static PApplet pAppletContext;
	private static int smallTileWidth;
	private static int smallTileHeight;
	private static int lowerScreenFactIndex;
	private static int numFramesSinceFactChange;
	public static int currentIndex;
	private static PImage img_fact1_English;
	private static PImage img_fact2_English;
	private static PImage img_fact3_English;
	private static PImage img_fact1_Spanish;
	private static PImage img_fact2_Spanish;
	private static PImage img_fact3_Spanish;
	private static PImage img_exit_English;
	private static PImage img_exit_Spanish;
	private static PImage img_progressBar_English;
	private static PImage img_progressBar_Spanish;
	private static final int MAX_NUM_FRAMES_BETWEEN_FACT_CHANGE = 200;
	private static final int NUM_FACTS = 3;
	
	
	public void Init(PApplet context, int tileWidth, int tileHeight, int numWithoutRepeats)
	{
		shuffledBoxes = new ArrayList<Box>();
		imageArray = new ArrayList<PImage>();
		currentIndex = 0;
		mostRecentIndices = new int[numWithoutRepeats]; 
		pAppletContext = context;
		smallTileWidth = tileWidth;
		smallTileHeight = tileHeight;
		lowerScreenFactIndex = 0;
		numFramesSinceFactChange = 0;
	}
	
	public void setStateImages(Vector<PImage> images)
	{
		// this is dumb
		img_fact1_English = images.get(0);
		img_fact2_English = images.get(1);
		img_fact3_English = images.get(2);
		img_fact1_Spanish = images.get(3);
		img_fact2_Spanish = images.get(4);
		img_fact3_Spanish = images.get(5);
		img_exit_English = images.get(6);
		img_exit_Spanish = images.get(7);
		img_progressBar_English = images.get(8);
		img_progressBar_Spanish = images.get(9);
	}
	
	public void setShuffledBoxes(ArrayList<Box> shuffled)
	{
		shuffledBoxes = shuffled;
	}
	
	
	public void setCategory(Photo[] photos, ArrayList<PImage> images)
	{
		this.setPhotoArray(photos);
		this.setImagesArray(images);
		currentIndex = 0;
	}
	
	public void setPhotoArray(Photo[] photos)
	{
		sourceArray = photos;
	}
	
	public void setImagesArray(ArrayList<PImage> images)
	{
		imageArray = images;
	}
	
	public void showNextImage(int languageChoice)
	{
		// calculate closest box
		
		for(int i = 0; i < 10; i++)
		{
			if(currentIndex > shuffledBoxes.size() - 1)
			{
				continue;
			}
			int iClosest = pickClosest(shuffledBoxes.get(currentIndex).img, sourceArray);
			
			// display it
			pAppletContext.image(imageArray.get(iClosest), shuffledBoxes.get(currentIndex).theX, shuffledBoxes.get(currentIndex).theY);
			currentIndex++;
		}
		
		numFramesSinceFactChange++;
		
		if(numFramesSinceFactChange > MAX_NUM_FRAMES_BETWEEN_FACT_CHANGE)
		{
			lowerScreenFactIndex = (lowerScreenFactIndex + 1) % NUM_FACTS;
			numFramesSinceFactChange = 0;
		}
		
		if (languageChoice == 1) {
			if (lowerScreenFactIndex == 0) {

				pAppletContext.image(img_fact1_English, 0, 1920);
			}
			if (lowerScreenFactIndex == 1) {
				pAppletContext.image(img_fact2_English, 0, 1920);
			}
			if (lowerScreenFactIndex == 2) {
				pAppletContext.image(img_fact3_English, 0, 1920);
			}
			
			pAppletContext.image(img_exit_English, 60, 2540);
			pAppletContext.image(img_progressBar_English, 0, 1920);

		
		} else {

			if (lowerScreenFactIndex == 0) {
				pAppletContext.image(img_fact1_Spanish, 0, 1920);
			}
			if (lowerScreenFactIndex == 1) {
				pAppletContext.image(img_fact2_Spanish, 0, 1920);
			}
			if (lowerScreenFactIndex == 2) {
				pAppletContext.image(img_fact3_Spanish, 0, 1920);
			}
			pAppletContext.image(img_exit_Spanish, 60, 2540);
			pAppletContext.image(img_progressBar_Spanish, 0, 1920);

		
		}
			
		
	}
	
	public boolean isMosaicSourceEmpty()
	{
		return shuffledBoxes.size() == 0;
	}
	
	public boolean isMosaicComplete()
	{
		return currentIndex >= shuffledBoxes.size();
	}
	
	public void ClearShuffledBoxes()
	{
		shuffledBoxes.clear();
	}
	
	private int pickClosest(PImage image, Photo[] photosArr)
	{
		int[] distArray1 = new int[photosArr.length];
		int[] distArray2 = new int[photosArr.length];
		int[] distArray3 = new int[photosArr.length];
		int[] distArray4 = new int[photosArr.length];
		int[] distArrayAll = new int[photosArr.length];
		PImage image1_1 = image.get(0, 0, smallTileWidth / 2, smallTileHeight / 2);
		PImage image2_1 = image.get(smallTileWidth / 2, 0, smallTileWidth / 2, smallTileHeight / 2);
		PImage image3_1 = image.get(0, smallTileHeight / 2, smallTileWidth / 2, smallTileHeight / 2);
		PImage image4_1 = image.get(smallTileWidth / 2, smallTileHeight / 2, smallTileWidth / 2,
				smallTileHeight / 2);

		for (int i = 0; i < photosArr.length; i++) {
			// int distSum = distanceBetween(image, photos_ST[i], 0);
			// distArray[i] = distSum;

			int distSum1 = distanceBetween(image1_1, photosArr[i], 1);
			distArray1[i] = distSum1;

			int distSum2 = distanceBetween(image2_1, photosArr[i], 2);
			distArray2[i] = distSum2;

			int distSum3 = distanceBetween(image3_1, photosArr[i], 3);
			distArray3[i] = distSum3;

			int distSum4 = distanceBetween(image4_1, photosArr[i], 4);
			distArray4[i] = distSum4;

			distArrayAll[i] = (distSum1 + distSum2 + distSum3 + distSum4) / 4;
		}
		
		int winner = 0;
		boolean doesntMatch = true;
		for (int k = 0; k < distArrayAll.length; k++) {
			for (int v = mostRecentIndices.length - 1; v >= 0; v--) {
				if (mostRecentIndices[v] == k) {
					doesntMatch = false;
					break;
				}
			}
			if (distArrayAll[k] < distArrayAll[winner] && doesntMatch) {
				winner = k;
				if(distArrayAll[winner] <= 30)
				{
					break; // try to improve efficiency by quitting when we find a value that's "good enough"
				}
			}
			doesntMatch = true;
		}
		

		for (int q = mostRecentIndices.length - 1; q > 0; q--) {
			mostRecentIndices[q] = mostRecentIndices[q - 1];
		}
		mostRecentIndices[0] = winner;

		return winner;
	}
	
	private int distanceBetween(PImage i1, Photo i2, int which) {
		float accumSum = 0;

		float sumRed1 = 0;
		float sumGreen1 = 0;
		float sumBlue1 = 0;

		for (int i8 = 0; i8 < i1.pixels.length; i8++) {
			sumRed1 += pAppletContext.red(i1.pixels[i8]);
			sumGreen1 += pAppletContext.green(i1.pixels[i8]);
			sumBlue1 += pAppletContext.blue(i1.pixels[i8]);
		}
		sumRed1 = sumRed1 / i1.pixels.length;

		sumGreen1 = sumGreen1 / i1.pixels.length;

		sumBlue1 = sumBlue1 / i1.pixels.length;

		switch (which) {
		case 0:
			accumSum = PApplet.dist(sumRed1, sumGreen1, sumBlue1, i2.r, i2.g, i2.b);
			break;
		case 1:
			accumSum = PApplet.dist(sumRed1, sumGreen1, sumBlue1, i2.r1, i2.g1, i2.b1);
			break;
		case 2:
			accumSum = PApplet.dist(sumRed1, sumGreen1, sumBlue1, i2.r2, i2.g2, i2.b2);
			break;
		case 3:
			accumSum = PApplet.dist(sumRed1, sumGreen1, sumBlue1, i2.r3, i2.g3, i2.b3);
			break;
		case 4:
			accumSum = PApplet.dist(sumRed1, sumGreen1, sumBlue1, i2.r4, i2.g4, i2.b4);
			break;
		}

		return PApplet.round(accumSum);
	}
}
